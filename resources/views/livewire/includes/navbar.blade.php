<div>
    <div class="row bg-warning">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <nav class="navbar navbar-expand-lg navbar-warning bg-warning">
                <img src="/images/logo.png" class="img-fluid">
            </nav>
        </div>
        <div class="col-lg-4"></div>
    </div>
    <div class="row bg-warning">
        <div class="col-lg-12">   
            <nav class="navbar navbar-expand-lg navbar-warning bg-warning">
                <div class="container-fluid">
                    
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">
                        </span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active text-light" aria-current="page" href="#">
                                    FESTIVAL <br>
                                    DE LA TRUFFE
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ms-5 text-light" href="#">
                                   NOS CARTES
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ms-5 text-light"href="#">
                                    CLICK & <br>
                                    COLLECT ROMA
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ms-5 text-light"href="#">
                                    LIVRAISON <br>
                                    AVEC WEDELY
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ms-5 text-light"href="#">
                                    NOS BANQUETS
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ms-5 text-light"href="#">
                                    CHÈQUES CADEAUX
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ms-5 text-light"href="#">
                                   GALERIE
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ms-5 text-light"href="#">
                                   CONTACT
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
