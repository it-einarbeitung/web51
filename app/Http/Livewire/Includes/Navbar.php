<?php

namespace App\Http\Livewire\Includes;

use Livewire\Component;

class Navbar extends Component
{
    public function render()
    {
        return view('livewire.includes.navbar');
    }
}
